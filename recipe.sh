#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

setup_environment()
{
    if [ ! -d depot_tools ]; then
        # retrieve Depot tools
        # https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up
        mkdir -p depot_tools
        pushd depot_tools
        wget -q https://storage.googleapis.com/chrome-infra/depot_tools.zip
        unzip -q -o depot_tools.zip
        popd
    fi

    # cipd_bin holds ninja.exe and other windows binaries
    export PATH="$(pwd)/depot_tools/.cipd_bin:$(pwd)/depot_tools:$PATH"

    # force to get local toolchain (not in Google)
    export DEPOT_TOOLS_WIN_TOOLCHAIN=0
}

checkout()
{
    if [ ! -d v8 ]; then
        cmd.exe /c "fetch v8"
        pushd v8
        git reset --hard $version
        git log -n1
        patch -p1 < $script_dir/0001-fix-arm64-compilation.patch
        popd
    fi
}

build()
{
    pushd v8
    mkdir -p out/arm64.release
    # https://chromium.googlesource.com/external/github.com/v8/v8.wiki/+/8c0be5e888bda68437f15e2ea9e317fd6229a5e3/Building-with-GN.md
    # https://bugs.chromium.org/p/v8/issues/detail?id=10365&q=%22windows%20on%20arm%22&can=2
    #
    # Available wrapper script (tools/dev/gm.py) does not work with arm64,
    # so directly add gn args instead.
    cat > out/arm64.release/args.gn << EOF
is_component_build = false
is_debug = false
v8_current_cpu = "arm64"
v8_target_cpu = "arm64"
target_cpu="arm64"
use_goma = false
goma_dir = "None"
v8_enable_backtrace = true
v8_enable_disassembler = true
v8_enable_object_print = true
v8_enable_verify_heap = true
is_clang=false # force using msvc
dcheck_always_on=true
EOF

    # clean env (conflicts with their setup, at vcvarsall.bat gets called again)
    # need to keep previous path (with depot_tools added)
    cat > build.bat << EOF
set __VSCMD_PREINIT_PATH=%PATH%
call vcvarsall.bat /clean_env || exit 1
python3 ../depot_tools/gn.py gen out/arm64.release || exit 1
EOF
    cmd /c "build.bat"
    ninja -C out/arm64.release
    file out/arm64.release/*.exe out/arm64.release/*.dll
    popd
}

setup_environment
checkout
build
